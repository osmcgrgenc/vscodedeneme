﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PAHEndustriel.Classes;
using System.IO;

namespace PAHEndustriel.UserControls
{
    public partial class LastReport : UserControl
    {
        int pointsplit = 0;

        public LastReport()
        {
            InitializeComponent();
            List<Control> allControls = Globals.GetAllControls(this);
            allControls.ForEach(k => k.Font = new System.Drawing.Font(Globals.fontFamilies[1], k.Font.Size));
            FillLastControl();
            //txtvalues.AppendText("Değer 1: Değer");
            //txtvalues.AppendText("\n");
            //txtvalues.SelectionColor = Color.Black;
            //
            //txtvalues.AppendText("Değer 2: Değer");
            //txtvalues.AppendText("\n");
            //txtvalues.SelectionColor = Color.Red;
            //
            //txtvalues.AppendText("Değer 3: Değer");
            //txtvalues.AppendText("\n");
            //txtvalues.SelectionColor = Color.Green;
            //
            //txtvalues.AppendText("Değer 4: Değer");
            //txtvalues.SelectionColor = Color.Purple;
            //txtvalues.AppendText("\n");
        }

        private int CheckLastControl()
        {
            return 0;
        }
        private int CheckLastControlbyProductName(string p_name)
        {
            return 0;
        }
        private void FillLastControl()
        {
            DataRow lastcontrol = Globals.GetDataRow(DBEnums.getLastControl);
            if (lastcontrol != null)
            {
                lblproductname.Text = lastcontrol["Product_Name"].ToString();
                      lblpname.Text = lastcontrol["Product_Name"].ToString();
                      lblpcode.Text = lastcontrol["Product_No"].ToString();
                       lbldate.Text = lastcontrol["DateTime"].ToString();
                       lbluser.Text = lastcontrol["UserInfo"].ToString();
                string val = lastcontrol["CheckingValues"].ToString();
                foreach (string item in val.Split('#'))
                {
                    txtvalues.AppendText(""+item.Split(':')[0]);
                    txtvalues.SelectionColor = Color.Red;
                    txtvalues.AppendText(":" + item.Split(':')[1]);
                    txtvalues.SelectionColor = Color.Black;
                    txtvalues.AppendText("\n");
                }
                DataTable ControlImages = DBconnection.getTable(DBEnums.getPicturesbyID.Replace("@KontrolID", " " + Convert.ToInt32(lastcontrol["ID"])));
                
                List<PictureBox> picList = new List<PictureBox>();
                picList.Add(pic_1); picList.Add(pic_2); picList.Add(pic_3);
                
                for (int i = 0; i < ControlImages.Rows.Count; i++)
                {
                    DataRow item = ControlImages.Rows[i];
                    Byte[] data = new Byte[0];
                    data = (Byte[])(item["Picture"]);
                    MemoryStream mem = new MemoryStream(data);
                    picList[i].Image=(Image.FromStream(mem));
                }

            }

        }
        private void splitter1_MouseDown(object sender, MouseEventArgs e)
        {
            splitter1.MouseMove += splitter1_MouseMove;
            pointsplit = e.X;
        }

        void splitter1_MouseMove(object sender, MouseEventArgs e)
        {
            int newpoint = e.X;

            panelimages.Width += pointsplit - newpoint;


        }

        private void splitter1_MouseUp(object sender, MouseEventArgs e)
        {
            splitter1.MouseMove -= splitter1_MouseMove;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAHEndustriel.Classes
{
    public class UserInfo
    {
        private int _id;
        private string _name;
        private string _firstname;
        private string _lastname;
        private int _role;
        private string _password;
        //1 kontrol calisani
        //2 kontrol mühendisi-müdürü
        //3 patron
        public int ID { get { return _id; } set { _id = value; } }
        public string Name { get { return _name; } set { _name = value; } }
        public string FirstName { get { return _firstname; } set { _firstname = value; } }
        public string LastName { get { return _lastname; } set { _lastname = value; } }
        public int Role { get { return _role; } set { _role = value; } }
        public string Password { get { return _password; } set { _password = value; } }
    }
}

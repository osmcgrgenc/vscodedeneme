﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAHEndustriel.Classes
{
    public class Product
    {
        public int ID;
        public string code;
        public string name;
        public Image image;
        public string values;
        public string imagestring;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PAHEndustriel.Classes;

namespace PAHEndustriel.UserControls
{
    public partial class AddNewProduct : UserControl
    {
        public AddNewProduct()
        {
            InitializeComponent();
            List<Control> allControls = Globals.GetAllControls(this);
            allControls.ForEach(k => k.Font = new System.Drawing.Font(Globals.fontFamilies[1], k.Font.Size));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            list_values.Items.Add(txt_value.Text);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog sfd = new OpenFileDialog();
            if (sfd.ShowDialog() == DialogResult.OK)
                {
                Image img = Image.FromFile(sfd.FileName);
                pic_teknik.Image = img;
                }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Product p = new Product();
            p.code = txt_pcode.Text;
            p.name = txt_pname.Text;
            p.values = "";
            foreach (var item in list_values.Items)
                {
                p.values = p.values + "" + item + "#";
                }
            p.image = pic_teknik.Image;
            //p.imagestring = Globals.ImageToBase64(p.image);
            if (Globals.InsertNewProductData(p))
                MessageBox.Show("Eklendi");
            else
                MessageBox.Show("Eklemedi");
        }
        
    }
}

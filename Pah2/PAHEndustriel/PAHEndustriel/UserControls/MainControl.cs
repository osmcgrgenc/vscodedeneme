﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PAHEndustriel.Classes;

namespace PAHEndustriel.UserControls
{
    public partial class MainControl : UserControl
    {
        public MainControl()
        {
            InitializeComponent();
            Timer chat = new Timer();
            chat.Interval = 1000;
            chat.Tick += chat_Tick;
            chat.Start();
            List<Control> allControls = Globals.GetAllControls(this);
            allControls.ForEach(k => k.Font = new System.Drawing.Font(Globals.fontFamilies[1], k.Font.Size));
            lblpcount.Text = "" + Globals.ProductList.Count;
            lblcontolcount.Text = Globals.GetDataRow(DBEnums.getControlCount)[0].ToString();
        }
        int lastmsgid = 0;
        void chat_Tick(object sender, EventArgs e)
        {
            DataTable chat = DBconnection.getTable(DBEnums.getMessages.Replace("@logid",""+lastmsgid));
            foreach (DataRow item in chat.Rows)
            {
                listView1.Items.Add(item["Message"].ToString());
                lastmsgid = Convert.ToInt32(item["ID"]);
            }
        }

        private void lbllastproduct_MouseEnter(object sender, EventArgs e)
        {
            (sender as Label).ForeColor = Color.OrangeRed;
        }

        private void lbllastproduct_MouseLeave(object sender, EventArgs e)
        {
            (sender as Label).ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Globals.InsertNewMessageGenel(richTextBox1.Text)) { 
                //listView1.Items.Add(richTextBox1.Text);
                richTextBox1.Text = "";
            }
        }
    }
}

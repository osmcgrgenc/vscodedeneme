﻿namespace PAHEndustriel.UserControls
{
    partial class LastReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblproductname = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtvalues = new System.Windows.Forms.RichTextBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panelimages = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.pic_2 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pic_3 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pic_1 = new System.Windows.Forms.PictureBox();
            this.lblpname = new System.Windows.Forms.Label();
            this.lblpcode = new System.Windows.Forms.Label();
            this.lbldate = new System.Windows.Forms.Label();
            this.lbluser = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panelimages.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_2)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_3)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblproductname);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(880, 64);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Impact", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(373, 63);
            this.label1.TabIndex = 2;
            this.label1.Text = "Son Alınan Rapor";
            // 
            // lblproductname
            // 
            this.lblproductname.AutoSize = true;
            this.lblproductname.Location = new System.Drawing.Point(414, 27);
            this.lblproductname.Name = "lblproductname";
            this.lblproductname.Size = new System.Drawing.Size(51, 13);
            this.lblproductname.TabIndex = 1;
            this.lblproductname.Text = "Ürün Adı!";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.comboBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(637, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(243, 64);
            this.panel2.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(152, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Ürün Seç";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 15);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.panelimages);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 64);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(880, 511);
            this.panel3.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lbluser);
            this.panel4.Controls.Add(this.lbldate);
            this.panel4.Controls.Add(this.lblpcode);
            this.panel4.Controls.Add(this.lblpname);
            this.panel4.Controls.Add(this.txtvalues);
            this.panel4.Controls.Add(this.splitter1);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(522, 511);
            this.panel4.TabIndex = 1;
            // 
            // txtvalues
            // 
            this.txtvalues.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtvalues.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvalues.DetectUrls = false;
            this.txtvalues.ForeColor = System.Drawing.Color.Maroon;
            this.txtvalues.Location = new System.Drawing.Point(29, 143);
            this.txtvalues.Name = "txtvalues";
            this.txtvalues.Size = new System.Drawing.Size(456, 284);
            this.txtvalues.TabIndex = 7;
            this.txtvalues.Text = "";
            // 
            // splitter1
            // 
            this.splitter1.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(516, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(6, 511);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            this.splitter1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.splitter1_MouseDown);
            this.splitter1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.splitter1_MouseUp);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(26, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 14);
            this.label5.TabIndex = 3;
            this.label5.Text = "Kontrolü Yapan:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(26, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 14);
            this.label4.TabIndex = 2;
            this.label4.Text = "Kontrol Tarihi:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(26, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 14);
            this.label3.TabIndex = 1;
            this.label3.Text = "Ürün Kodu:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(26, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Ürün Adı:";
            // 
            // panelimages
            // 
            this.panelimages.Controls.Add(this.panel7);
            this.panelimages.Controls.Add(this.panel6);
            this.panelimages.Controls.Add(this.panel5);
            this.panelimages.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelimages.Location = new System.Drawing.Point(522, 0);
            this.panelimages.Name = "panelimages";
            this.panelimages.Size = new System.Drawing.Size(358, 511);
            this.panelimages.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.pic_2);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 100);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(358, 311);
            this.panel7.TabIndex = 2;
            // 
            // pic_2
            // 
            this.pic_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic_2.Location = new System.Drawing.Point(0, 0);
            this.pic_2.Name = "pic_2";
            this.pic_2.Size = new System.Drawing.Size(358, 311);
            this.pic_2.TabIndex = 0;
            this.pic_2.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.pic_3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 411);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(358, 100);
            this.panel6.TabIndex = 1;
            // 
            // pic_3
            // 
            this.pic_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic_3.Location = new System.Drawing.Point(0, 0);
            this.pic_3.Name = "pic_3";
            this.pic_3.Size = new System.Drawing.Size(358, 100);
            this.pic_3.TabIndex = 0;
            this.pic_3.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.pic_1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(358, 100);
            this.panel5.TabIndex = 0;
            // 
            // pic_1
            // 
            this.pic_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic_1.Location = new System.Drawing.Point(0, 0);
            this.pic_1.Name = "pic_1";
            this.pic_1.Size = new System.Drawing.Size(358, 100);
            this.pic_1.TabIndex = 0;
            this.pic_1.TabStop = false;
            // 
            // lblpname
            // 
            this.lblpname.AutoSize = true;
            this.lblpname.Location = new System.Drawing.Point(195, 19);
            this.lblpname.Name = "lblpname";
            this.lblpname.Size = new System.Drawing.Size(35, 13);
            this.lblpname.TabIndex = 8;
            this.lblpname.Text = "label6";
            // 
            // lblpcode
            // 
            this.lblpcode.AutoSize = true;
            this.lblpcode.Location = new System.Drawing.Point(195, 45);
            this.lblpcode.Name = "lblpcode";
            this.lblpcode.Size = new System.Drawing.Size(35, 13);
            this.lblpcode.TabIndex = 9;
            this.lblpcode.Text = "label7";
            // 
            // lbldate
            // 
            this.lbldate.AutoSize = true;
            this.lbldate.Location = new System.Drawing.Point(195, 71);
            this.lbldate.Name = "lbldate";
            this.lbldate.Size = new System.Drawing.Size(35, 13);
            this.lbldate.TabIndex = 10;
            this.lbldate.Text = "label8";
            // 
            // lbluser
            // 
            this.lbluser.AutoSize = true;
            this.lbluser.Location = new System.Drawing.Point(195, 97);
            this.lbluser.Name = "lbluser";
            this.lbluser.Size = new System.Drawing.Size(35, 13);
            this.lbluser.TabIndex = 11;
            this.lbluser.Text = "label9";
            // 
            // LastReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Name = "LastReport";
            this.Size = new System.Drawing.Size(880, 575);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panelimages.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_2)).EndInit();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_3)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblproductname;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelimages;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pic_3;
        private System.Windows.Forms.PictureBox pic_1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox pic_2;
        private System.Windows.Forms.RichTextBox txtvalues;
        private System.Windows.Forms.Label lbluser;
        private System.Windows.Forms.Label lbldate;
        private System.Windows.Forms.Label lblpcode;
        private System.Windows.Forms.Label lblpname;
        
    }
}

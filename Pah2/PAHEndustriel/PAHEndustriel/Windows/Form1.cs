﻿using PAHEndustriel.Classes;
using PAHEndustriel.Windows;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PAHEndustriel
{
    public partial class Form1 : Form
    {
        #region dragmove
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
                         int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        #endregion
        List<ProgramSettings> ps = new List<ProgramSettings>();
        SettingsSerializer ser = new SettingsSerializer();
        public Form1()
        {
            fontfamiles();
            InitializeComponent();
            try
            {
                
                ps = ser.DeserializePortSettings();
                Globals._settings = ps[0];
            }
            catch 
            {
                ps.Add(new ProgramSettings() {DBAdress="localhost", DBName="KaliteKontrol",DBUserName="",DBPassword="" });
                ser.SerializePortSettings(ps);
                ps = ser.DeserializePortSettings();
                if (ps == null || ps.Count == 0)
                    Globals._settings = ps[0];
            }
            label1.Font = new Font(Globals.fontFamilies[1], label1.Font.Size);
            label2.Font = new Font(Globals.fontFamilies[1], label2.Font.Size);
            button1.Font = new Font(Globals.fontFamilies[1], button1.Font.Size);
            button2.Font = new Font(Globals.fontFamilies[1], button2.Font.Size);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CheckLogin();
        }
        private void CheckLogin() 
        {
            if (textBox1.Text.Equals("")) { textBox1.Focus(); }
            else if (textBox2.Text.Equals("")) { textBox2.Focus(); }
            else
            {
                if (Globals.CheckUser(textBox1.Text,textBox2.Text)) { Dashboard db = new Dashboard(this); db.Show(); this.Hide(); }
                else MessageBox.Show("Hatalı kullanıcı adı veya şifre girdiniz.");
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                CheckLogin();
        }
        PrivateFontCollection privateFontCollection = new PrivateFontCollection();

        //Select your font from the resources.
        //My font here is "Digireu.ttf"
        int fontLength = Properties.Resources._3ds_Italic.Length;
        
        // create a buffer to read in to
        byte[] fontdata = Properties.Resources._3ds_Italic;

        // create an unsafe memory block for the font data

        void fontfamiles() 
        {
            

            int count = 0;
            string familyName = "";
            string familyNameAndStyle;
            
            

            // Add three font files to the private collection.
            privateFontCollection.AddFontFile(@"Resources\3ds Bold.ttf");
            privateFontCollection.AddFontFile(@"Resources\3ds Condensed Bold.ttf");
            privateFontCollection.AddFontFile(@"Resources\3ds Condensed Light.ttf");
            privateFontCollection.AddFontFile(@"Resources\3ds Condensed Regular.ttf");
            privateFontCollection.AddFontFile(@"Resources\3ds ExtraLight.ttf");
            privateFontCollection.AddFontFile(@"Resources\3ds Italic.ttf");
            privateFontCollection.AddFontFile(@"Resources\3ds Light.ttf");
            privateFontCollection.AddFontFile(@"Resources\3ds Regular.ttf");
            privateFontCollection.AddFontFile(@"Resources\3ds SemiBold.ttf");
            

            // Get the array of FontFamily objects.
            Globals.fontFamilies = privateFontCollection.Families;

            // How many objects in the fontFamilies array?
            count = Globals.fontFamilies.Length;

            // Display the name of each font family in the private collection
            // along with the available styles for that font family.
            for (int j = 0; j < count; ++j)
            {
                // Get the font family name.
                familyName = Globals.fontFamilies[j].Name;

                // Is the regular style available?
                if (Globals.fontFamilies[j].IsStyleAvailable(FontStyle.Regular))
                {
                    familyNameAndStyle = "";
                    familyNameAndStyle = familyNameAndStyle + familyName;
                    familyNameAndStyle = familyNameAndStyle + " Regular";

                    Font regFont = new Font(
                       familyName,
                       16,
                       FontStyle.Regular,
                       GraphicsUnit.Pixel);

                    

                    
                }

                // Is the bold style available?
                if (Globals.fontFamilies[j].IsStyleAvailable(FontStyle.Bold))
                {
                    familyNameAndStyle = "";
                    familyNameAndStyle = familyNameAndStyle + familyName;
                    familyNameAndStyle = familyNameAndStyle + " Bold";

                    Font boldFont = new Font(
                       familyName,
                       16,
                       FontStyle.Bold,
                       GraphicsUnit.Pixel);

                    
                }
                // Is the italic style available?
                if (Globals.fontFamilies[j].IsStyleAvailable(FontStyle.Italic))
                {
                    familyNameAndStyle = "";
                    familyNameAndStyle = familyNameAndStyle + familyName;
                    familyNameAndStyle = familyNameAndStyle + " Italic";

                    Font italicFont = new Font(
                       familyName,
                       16,
                       FontStyle.Italic,
                       GraphicsUnit.Pixel);

                    
                }

                // Is the bold italic style available?
                if (Globals.fontFamilies[j].IsStyleAvailable(FontStyle.Italic) &&
               Globals.fontFamilies[j].IsStyleAvailable(FontStyle.Bold))
                {
                    familyNameAndStyle = "";
                    familyNameAndStyle = familyNameAndStyle + familyName;
                    familyNameAndStyle = familyNameAndStyle + "BoldItalic";

                    Font italicFont = new Font(
                       familyName,
                       16,
                       FontStyle.Italic | FontStyle.Bold,
                       GraphicsUnit.Pixel);

                    
                }
                // Is the underline style available?
                if (Globals.fontFamilies[j].IsStyleAvailable(FontStyle.Underline))
                {
                    familyNameAndStyle = "";
                    familyNameAndStyle = familyNameAndStyle + familyName;
                    familyNameAndStyle = familyNameAndStyle + " Underline";

                    Font underlineFont = new Font(
                       familyName,
                       16,
                       FontStyle.Underline,
                       GraphicsUnit.Pixel);

                    
                }

                // Is the strikeout style available?
                if (Globals.fontFamilies[j].IsStyleAvailable(FontStyle.Strikeout))
                {
                    familyNameAndStyle = "";
                    familyNameAndStyle = familyNameAndStyle + familyName;
                    familyNameAndStyle = familyNameAndStyle + " Strikeout";

                    Font strikeFont = new Font(
                       familyName,
                       16,
                       FontStyle.Strikeout,
                       GraphicsUnit.Pixel);

                  
                }

                // Separate the families with white space.
                

            }
        }
        
    }
}

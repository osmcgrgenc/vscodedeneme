﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAHEndustriel.Classes
{
    public static class DBconnection
    {
        private static string ConnectionString = "Data Source="+Globals._settings.DBAdress +
                                                 ";User ID="+Globals._settings.DBUserName+
                                                 " ; Password="+Globals._settings.DBPassword +
                                                 "; Initial Catalog="+Globals._settings.DBName+
                                                 " ; Integrated Security=True;Connect Timeout=15 ; Encrypt=False ; TrustServerCertificate=False ;";
        public static SqlConnection sqlConect()
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(GetSqlConnectionString());
                sqlConnection.Open();
                return sqlConnection;
            }
            catch { return null; }
            
        }
        static private string GetSqlConnectionString()
        {
            // Prepare the connection string to Azure SQL Database.  
            var sqlConnectionSB = new SqlConnectionStringBuilder();

            // Change these values to your values.  
            sqlConnectionSB.DataSource = "tcp:osmcgrgenc.database.windows.net,14343"; //["Server"]  
            sqlConnectionSB.InitialCatalog = "KaliteKontrol"; //["Database"]  

            sqlConnectionSB.UserID = "cagrigenc";  // "@yourservername"  as suffix sometimes.  
            sqlConnectionSB.Password = "Cagri_1994";
            sqlConnectionSB.IntegratedSecurity = false;

            //// Adjust these values if you like. (ADO.NET 4.5.1 or later.)  
            //sqlConnectionSB.ConnectRetryCount = 3;
            //sqlConnectionSB.ConnectRetryInterval = 10;  // Seconds.  

            // Leave these values as they are.  
            sqlConnectionSB.IntegratedSecurity = false;
            sqlConnectionSB.Encrypt = true;
            sqlConnectionSB.ConnectTimeout = 30;

            return sqlConnectionSB.ToString();
        }  
        public static DataTable getTable(string query)
        {
            try
            {
            SqlConnection connect = sqlConect();
            SqlDataAdapter adapter = new SqlDataAdapter(query, connect);
            DataTable dt = new DataTable();

            
                adapter.Fill(dt);
            

            adapter.Dispose();
            connect.Close();
            connect.Dispose();
            //ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            //ConnectionString = ConnectionString.Replace("@password", EncryptionClass.Encryption.Decrypt(Properties.Settings.Default.DBPassword));

            return dt;
            }
            catch (Exception ex)
            {
                
                return null;
            }
        }
        public static DataTable GetTable(string query)
        {
            SqlConnection sqlConnection = sqlConect();
            SqlDataAdapter adapter = new SqlDataAdapter(query, sqlConnection);
            DataTable dt = new DataTable();

            try
            {
                adapter.Fill(dt);
            }
            catch (SqlException ex)
            {
                // MessageBox.Show("hata : " + ex.Message + "(" + query + ")");
                throw new Exception("Get Table" + ex.Message + "(" + query + ")");
            }

            adapter.Dispose();
            sqlConnection.Close();
            sqlConnection.Dispose();

            return dt;
        }
        public static DataSet SetAl(string query)
        {
            SqlConnection connect = sqlConect();
            SqlDataAdapter adapter = new SqlDataAdapter(query, connect);
            DataSet ds = new DataSet();
            try
            {
                adapter.Fill(ds);
            }
            catch (SqlException ex)
            {
                //MessageBox.Show("Error : " + ex.Message + "(" + query + ")");
                throw new Exception("Set Al Problem !" + ex.Message + "(" + query + ")");
            }
            adapter.Dispose();
            connect.Close();
            connect.Dispose();
            return ds;
        }
        public static DataRow GetRow(string query)
        {
            DataTable dt = getTable(query);
            if (dt.Rows.Count == 0)
                return null;
            else
                return dt.Rows[0];
        }
        public static string GetCell(string query)
        {
            DataTable dt = getTable(query);
            if (dt.Rows.Count == 0)
                return null;
            else
                return dt.Rows[0][0].ToString();
        }

        public static int ExecuteCommand(string query,params object[] prm)
        {
            SqlParameter parameter;
            SqlConnection sqlConnection = sqlConect();
            //   SqlTransaction trans = baglan.BeginTransaction();
            SqlCommand command = new SqlCommand(query, sqlConnection);
            if(prm.Length==2)
                parameter = command.Parameters.AddWithValue(prm[0].ToString(), prm[1]);
             
               
            int result = 0;

            try
            {
                result = command.ExecuteNonQuery();
                //       trans.Commit();
            }
            catch (SqlException ex)
            {

                // MessageBox.Show("hata : " + ex.Message + "(" + sorgu + ")");
                //   trans.Rollback();
                Console.WriteLine("Execute Command Problem" + ex.Message + "(" + query + ")");
                //throw new Exception("Execute Command Problem" + ex.Message + "(" + query + ")");
            }

            command.Dispose();
            sqlConnection.Close();
            sqlConnection.Dispose();

            return result;

        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAHEndustriel.Classes
{
    public class ProgramSettings
    {
        #region DB
        private string _DBname;
        private string _DBusername;
        private string _DBpassword;
        private string _DBadress;
        #endregion
        public string DBName { get { return _DBname; } set { _DBname = value; } }
        public string DBUserName { get { return _DBusername; } set { _DBusername = value; } }
        public string DBPassword { get { return _DBpassword; } set { _DBpassword = value; } }
        public string DBAdress { get { return _DBadress; } set { _DBadress = value; } }

    }
}

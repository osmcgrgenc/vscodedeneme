﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PAHEndustriel.Classes
    {
    public static class Globals
        {
        public static List<Product> ProductList;
        public static ProgramSettings _settings;
        public static UserInfo user;
        public static FontFamily[] fontFamilies;
        public static bool CheckUser(string name, string password)
            {
            DataRow userrow = GetDataRow("Select * From users Where username='" + name + "' and password='" + password + "'");
            if (userrow != null)
                {
                user = new UserInfo();
                user.ID = Convert.ToInt32(userrow["ID"]);
                user.Name = (userrow["UserName"]).ToString();
                user.FirstName = (userrow["FirstName"]).ToString();
                user.LastName = (userrow["LastName"]).ToString();
                user.Password = (userrow["Password"]).ToString();
                user.Role = Convert.ToInt32(userrow["Role"]);
                return true;
                }
            return false;
            }
        public static string UserFirstLastName()
            {
            return user.FirstName + " " + user.LastName;
            }
        public static DataRow GetDataRow(string query)
            {
            return DBconnection.GetRow(query);
            }
        public static bool InsertNewProductData(Product p)
            {
            string query = "INSERT INTO KaliteKontrol.dbo.Products"+
	                        " ( Product_No, Product_Name, Product_Values, Product_Image) "+
                           "values ('" + p.code + "','" + p.name + "','" + p.values + "'," +"@Image" + ")";

            int result = DBconnection.ExecuteCommand(query,"@Image", ImageToByteArray(p.image));
            if (result > 0)
                return true;
            else
                return false;
            }
        public static bool InsertNewMessageGenel(string message) 
        {
            string query = "INSERT INTO KaliteKontrol.dbo.Messages" +
                            " (  User_Sender_ID, User_Receiver_ID, DateTime, Message) " +
                           "values ('" + user.ID + "','0','" + DateTime.Now + "','" + message + "')";

            int result = DBconnection.ExecuteCommand(query);
            if (result > 0)
                return true;
            
            return false;
        }
        public static byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }
        public static List<Control> GetAllControls(Control container, List<Control> list)
            {
            foreach (Control c in container.Controls)
                {

                if (c.Controls.Count > 0)
                    list = GetAllControls(c, list);
                else
                    list.Add(c);
                }

            return list;
            }
        public static List<Control> GetAllControls(Control container)
            {
            return GetAllControls(container, new List<Control>());
            }
        public static string ImageToBase64(Image img)
        {
            using (Image image = img)
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();

                    // Convert byte[] to Base64 String
                    string base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
        }
        public static void FillProductList() 
        {
            ProductList = new List<Product>();
            DataTable table = DBconnection.getTable(DBEnums.getProductList);
            foreach (DataRow item in table.Rows)
            {
                Product pr = new Product();
                pr.ID = Convert.ToInt32(item["ID"]);
                pr.name = item["Product_Name"].ToString();
                pr.values = item["Product_Values"].ToString();
                pr.code = item["Product_No"].ToString();
                Byte[] data = new Byte[0];
                data = (Byte[])(item["Product_Image"]);
                MemoryStream mem = new MemoryStream(data);
                pr.image = (Image.FromStream(mem));
                ProductList.Add(pr);
            }
        }
        }

    }

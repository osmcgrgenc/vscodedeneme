﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAHEndustriel.Classes
{
    public static class DBEnums
    {
        public static string getControls = "SELECT qc.[ID]" +
                                                    ",p.Product_No," +
                                                    "p.Product_Name" +
                                                    ",[DateTime]" +
                                                    ",[CheckingValues]" +
                                                    ",u.FirstName+' '+u.LastName as UserInfo " +
                                                    "FROM [dbo].[QualityControls] as qc " +
                                                    "Inner join Products as p " +
                                                    "On qc.Product_ID=p.ID " +
                                                    "Inner join Users as u " +
                                                    "on qc.User_ID=u.ID";
        public static string getLastControl = "SELECT top 1 qc.[ID]"+
		                                            ",p.Product_No,"+
		                                            "p.Product_Name"+
		                                            ",[DateTime]"+
		                                            ",[CheckingValues]"+
		                                            ",u.FirstName+' '+u.LastName as UserInfo "+
		                                            "FROM [dbo].[QualityControls] as qc "+
		                                            "Inner join Products as p "+
		                                            "On qc.Product_ID=p.ID "+
		                                            "Inner join Users as u "+
		                                            "on qc.User_ID=u.ID "+
		                                            "order by qc.ID desc";
        public static string getPicturesbyID = "select Picture from Images where Control_ID = @KontrolID ";
        public static string getProductList = "select * from Products";
        public static string getMessages = "select top 50 * from Messages where id > @logid order by ID desc";
        public static string getControlCount = "select Count(ID) from QualityControls";
    }
}

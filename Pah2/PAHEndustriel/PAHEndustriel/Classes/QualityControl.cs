﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAHEndustriel.Classes
{
    public class QualityControl
    {
        int _id;
        int _p_id;
        int _u_id;
        DateTime _datetime;
        string _values;
        byte[] _image;
        public int ID { get; set; }
        public int Product_ID { get; set; }
        public int User_ID { get; set; }
        public DateTime DateTime { get; set; }
        public byte[] Image { get; set; }
        public string CheckingValues { get; set; }
    }
}

﻿using PAHEndustriel.Classes;
using PAHEndustriel.UserControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PAHEndustriel.Windows
{
    public partial class Dashboard : Form
    {
        #region dragmove
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
                         int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        #endregion
        
        #region variables
        Form1 _parent;
        Timer timer_dt;
        #endregion
        public Dashboard(Form1 parent)
        {
            timer_dt = new Timer();
            timer_dt.Tick += timer_dt_Tick;
            timer_dt.Interval = 1000;            
            _parent = parent;
            this.Load += Dashboard_Load;
            InitializeComponent();
            List<Control> allControls = Globals.GetAllControls(this);
            allControls.ForEach(k => k.Font = new System.Drawing.Font(Globals.fontFamilies[1], k.Font.Size));
        }
        
       

        void Dashboard_Load(object sender, EventArgs e)
        {
            Globals.FillProductList();
            lblnickname.Text = Globals.UserFirstLastName();
            lbldate.Text = DT_string();
            timer_dt.Start();
            MainControl mc = new MainControl();
            mc.Size = container.Size;
            container.Controls.Add(mc);
        }
        string DT_string() 
        {
            return DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
        }
        void timer_dt_Tick(object sender, EventArgs e)
        {
            lbldate.Text = DT_string();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void panel2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.WindowState != FormWindowState.Maximized)
                this.WindowState = FormWindowState.Maximized;
            else
                this.WindowState = FormWindowState.Normal;
            if (container.Controls.Count > 0)
                container.Controls[0].Size = container.Size;
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void label1_MouseEnter(object sender, EventArgs e)
        {
            (sender as Label).ForeColor = Color.White;
        }

        private void label2_MouseLeave(object sender, EventArgs e)
        {
            (sender as Label).ForeColor = Color.Black;
        }

        private void Dashboard_FormClosing(object sender, FormClosingEventArgs e)
        {
            _parent.Show();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            if (panel1.Visible)
            {
                label3.Text = ">";
                panel1.Visible = false;
            }
            else { label3.Text = "<"; panel1.Visible = true; }
            if (container.Controls.Count > 0)
                container.Controls[0].Size = container.Size;
        }

        private void label4_MouseEnter(object sender, EventArgs e)
        {
            (sender as Label).ForeColor = Color.Orange;
        }

        private void label4_MouseLeave(object sender, EventArgs e)
        {
            (sender as Label).ForeColor = Color.White;
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            (sender as Button).ForeColor = Color.OrangeRed;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            (sender as Button).ForeColor = Color.Black;
        }

        private void txtsource_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                LastReport mc = new LastReport();
                mc.Size = container.Size;
                if (container.Controls.Count > 0) container.Controls.Clear();
                container.Controls.Add(mc);
            }
            else
            {
                string search = txtsource.Text;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Reports rep = new Reports();
            rep.Size = container.Size;
            if (container.Controls.Count > 0) container.Controls.Clear();
            container.Controls.Add(rep);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MainControl mc = new MainControl();
            mc.Size = container.Size;
            if (container.Controls.Count > 0) container.Controls.Clear();
            container.Controls.Add(mc);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            LastReport mc = new LastReport();
            mc.Size = container.Size;
            if (container.Controls.Count > 0) container.Controls.Clear();
            container.Controls.Add(mc);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            AddNewProduct anp = new AddNewProduct();
            anp.Size = container.Size;
            if (container.Controls.Count > 0) container.Controls.Clear();
            container.Controls.Add(anp);
        }
    }
}

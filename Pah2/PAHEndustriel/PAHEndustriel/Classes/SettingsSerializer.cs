﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PAHEndustriel.Classes
{
    class SettingsSerializer
    {
        XmlSerializer s;
        public void SerializePortSettings(List<ProgramSettings> settings)
        {
            s = new XmlSerializer(typeof(List<ProgramSettings>));
            settings.Add(new ProgramSettings());
            using (var stream = File.OpenWrite(@"Resources\SerialSettings.xml"))
            {
                s.Serialize(stream, settings);
            }
        }

        public List<ProgramSettings> DeserializePortSettings()
        {
            var settings = new List<ProgramSettings>();

            var serializer = new XmlSerializer(typeof(List<ProgramSettings>));
            using (var stream = File.OpenRead(@"Resources\SerialSettings.xml"))
            {
                var other = (List<ProgramSettings>)(serializer.Deserialize(stream));
                settings.Clear();
                settings.AddRange(other);
            }
            return settings;

        }
        
    }
}
